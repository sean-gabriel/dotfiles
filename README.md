# About #

This is a repository to keep my home directory dotfiles synced across machines.
This configuration avoids turning your home directory into a git repository root,
without resorting to symlinks. Includes a convenience script for running git pull.

## Setup ##

```
#!bash

git clone --no-checkout git@bitbucket.org:sean-gabriel/dotfiles.git ~/.dotfiles
cd ~/.dotfiles
git config core.worktree ../..
git checkout
./pull
```