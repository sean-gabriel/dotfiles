#!/bin/sh

function color {
  case "$1" in
    reset)  echo '\[\e[0m\]' ;;
    red)    echo '\[\e[31m\]' ;;
    green)  echo '\[\e[32m\]' ;;
    yellow) echo '\[\e[33m\]' ;;
    cyan)   echo '\[\e[36m\]' ;;
  esac
}

function set_prompt {
  local dirty=0
  local branch=''
  local status="$( git status 2>&1 )"
  local branch_regex='On branch ([^[:space:]]+)'
  local nogit_regex='^fatal: Not a git repository'
  local clean_regex='nothing to commit'

  if [[ ! $status =~ $nogit_regex ]]; then
    if [[ $status =~ $branch_regex ]]; then
      branch=${BASH_REMATCH[1]}
    fi

    if [[ ! $status =~ $clean_regex ]]; then
      dirty=1
    fi
  fi

  PS1="\[\e]0;localhost:\w\a\]"
  PS1="${PS1}$(color cyan)localhost$(color reset):$(color yellow)\w"

  if [ $branch ]; then
    if [ $dirty == 1 ]; then
      PS1="${PS1}$(color reset)|$(color red)${branch}"
    else
      PS1="${PS1}$(color reset)|$(color green)${branch}"
    fi
  fi

  PS1="${PS1}$(color reset)\$ "
}

export PROMPT_COMMAND=set_prompt

